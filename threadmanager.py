import sys
import time
import datetime
import numpy as np
import struct
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import hid

class ThreadManager(object):
    '''
    Allow for (re)starting threads, avoid closing/opening HID when settings change
    '''
    def __init__(self):
        self.dev = hid.device()
        self.dev.open(0x16c0, 0x486)
        self.dev.set_nonblocking(1)
        self.thread = None
        self.logging = False
        self.printing = False
    
    def start(self, small_buffer_size, big_buffer_size, raw, forces, errors, threedee):
        small_buffer = np.full((int(small_buffer_size), 22), np.nan)
        big_buffer = np.full((int(big_buffer_size), 22), np.nan)
        force_buffer = np.full((int(big_buffer_size), 17), np.nan)
        error_buffer = np.full((int(big_buffer_size), 7), np.nan)
        # restart thread if it already exists
        if isinstance(self.thread, Thread):
            self.thread.stop()
        self.thread = Thread(self.dev, small_buffer, big_buffer, force_buffer, error_buffer, self.logging, self.printing)
        self.thread.raw_data.connect(raw)
        self.thread.force_data.connect(forces)
        self.thread.threedee_data.connect(threedee)
        self.thread.error_data.connect(errors)
        self.thread.start()
    def stop(self):
        self.thread.stop()
    def toggle_print(self, state):
        self.printing = state
        self.thread._isprinting = state
    def toggle_log(self, state):
        self.logging = state


class Thread(pg.QtCore.QThread):
    raw_data = pg.QtCore.Signal(object)
    force_data = pg.QtCore.Signal(object)
    threedee_data = pg.QtCore.Signal(object)
    error_data = pg.QtCore.Signal(object)

    def __init__(self, device, small_buffer, big_buffer, force_buffer, error_buffer, logging, printing):
        super(Thread, self).__init__()
        self._isrunning = True
        self._islogging = logging
        self._isprinting = printing
        self.small_buffer = small_buffer
        self.big_buffer = big_buffer
        self.force_buffer = force_buffer
        self.error_buffer = error_buffer
        self.dev = device
        self.rotation_val = np.pi / 4
    
    def run(self):
        self.setPriority(pg.QtCore.QThread.HighPriority)
        logging_this_session = False
        if self._islogging:
            logging_this_session = True
            filename = datetime.datetime.now().strftime('log_%Y-%m-%d_%H-%M-%S.txt')
        # clear old readings
        for i in range(30):
            self.dev.read(64)
        
        while self._isrunning:
            self.small_buffer.fill(np.nan)
            while np.isnan(self.small_buffer).any():
                try:
                    data = bytearray(self.dev.read(46))
                    if data:
                        data = struct.unpack('>LhHHHHHHHHHHHHHHHHHHHH', data)
                        if self._isprinting:
                            print(data)
                        next_row = np.where(np.isnan(self.small_buffer).any(axis=1))[0][0]
                        self.small_buffer[next_row,:] = data
                except IOError:
                    sys.exit('Finger device disconnected.')
            self.small_buffer[:,0] /= 1000.0
            self.small_buffer[:,2:] /= 65535.0
            if logging_this_session:
                with open(filename, 'ab') as file:
                    np.savetxt(file, self.small_buffer, fmt='%10.3f', delimiter='\t')
            data_rows = self.small_buffer.shape[0]
            nan_rows = np.isnan(self.big_buffer).any(axis=1)
            # update plot
            if any(nan_rows):
                inds = np.where(nan_rows)[0][0]
                self.big_buffer[inds:(inds+data_rows),:] = self.small_buffer
            else:
                self.big_buffer = np.roll(self.big_buffer, -data_rows, axis=0)
                self.big_buffer[-data_rows:self.big_buffer.shape[0],:] = self.small_buffer
            self.raw_data.emit(self.big_buffer)

            # compute x,y,z here
            self.force_buffer[:,0:2] = self.big_buffer[:,0:2] # timestamp & delta
            self.force_buffer[:,2:17:3] = self.big_buffer[:,2:22:4] * np.cos(self.rotation_val) - self.big_buffer[:,3:22:4] * np.sin(self.rotation_val) #x
            self.force_buffer[:,3:17:3] = self.big_buffer[:,2:22:4] * np.sin(self.rotation_val) + self.big_buffer[:,3:22:4] * np.cos(self.rotation_val)#y
            self.force_buffer[:,4:17:3] = self.big_buffer[:,4:22:4] + self.big_buffer[:,5:22:4] # z
            # z' == z
            self.force_data.emit(self.force_buffer)
            self.threedee_data.emit(self.force_buffer)

            # compute error
            self.error_buffer[:,0:2] = self.big_buffer[:,0:2]
            self.error_buffer[:,2:7] = np.abs(self.big_buffer[:,2:22:4] - self.big_buffer[:,3:22:4] + self.big_buffer[:,4:22:4] - self.big_buffer[:,5:22:4])
            self.error_data.emit(self.error_buffer)
            time.sleep(0.00001)
    
    def stop(self):
        self._isrunning = False

