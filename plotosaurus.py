import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.opengl as gl
from pyqtgraph.dockarea import DockArea, Dock

# local files (for readability's sake)
from plotwindows import PlotWindow
from threadmanager import ThreadManager, Thread
from threedee import ThreeDeePlot

# default values
sampling_freq = 1000 # in Hz (cycles/sec)
xaxis_lim = 2 # in seconds
force_val = 5
percentage = 0.02

# base window, etc.
app = QtGui.QApplication([])
win = QtGui.QMainWindow()
#win.setWindowIcon(QtGui.QIcon('icon.jpg'))
area = DockArea()
win.setCentralWidget(area)
win.resize(1200, 400)
win.setWindowTitle('The Plotosaurus')

# widgets --
# 1. (normalized) voltages per finger (tabbed)
# 2. forces per finger (tabbed)
# 3. (tbd) 3d plot of force per finger (tabbed)
# 4. settings (force range, sampling freq, plot x-axis size)
d1 = Dock('Normalized Voltages', size = (1000, 600))
d2 = Dock('Forces', size = (1000, 600))
d3 = Dock('Errors', size = (1000, 600))
d4 = Dock('ThreeDee', size = (1000, 600))
d5 = Dock('Settings', size = (250, 600))

area.addDock(d1, 'left')
area.addDock(d2, 'left')
area.addDock(d3, 'left')
area.addDock(d4, 'left')
area.addDock(d5, 'right')

area.moveDock(d3, 'above', d4)
area.moveDock(d2, 'above', d3)
area.moveDock(d1, 'above', d2) # stack plots by default

w1 = PlotWindow(sampling_freq=sampling_freq, xaxis_lim=xaxis_lim, type='raw')
d1.addWidget(w1)
## Trace force plot
w2 = PlotWindow(sampling_freq=sampling_freq, xaxis_lim=xaxis_lim, type='force')
d2.addWidget(w2)
w3 = PlotWindow(sampling_freq=sampling_freq, xaxis_lim=xaxis_lim, type='error')
d3.addWidget(w3)

w4 = ThreeDeePlot(sampling_freq=sampling_freq, xaxis_lim=xaxis_lim)
d4.addWidget(w4)

## settings widget
w5 = pg.LayoutWidget()

lab0 = QtGui.QLabel('Force Range (N):')
force_select = QtGui.QComboBox()
force_select.addItem('1') # access via force_select.currentText()
force_select.addItem('5')
force_select.addItem('20')
force_select.setCurrentText(str(force_val))
lab1 = QtGui.QLabel('Sampling Frequency (Hz):')
# access via freq_select.value()
freq_select = pg.SpinBox(value=sampling_freq, bounds=[1, 1000], int=True, dec=True, minStep=10, decimals=4)
lab2 = QtGui.QLabel('X-Axis limits (sec):')
window_select = pg.SpinBox(value=xaxis_lim, bounds=[0.5, 10], int=False, dec=True, minStep=0.1)
apply_button = QtGui.QPushButton('Apply Settings')
zero_button = QtGui.QPushButton('Center Traces')

def print_to_console(state):
    thread_manager.toggle_print(state)

def toggle_log(state):
    thread_manager.toggle_log(state)

logging_checkbox = QtGui.QCheckBox('log (on apply)')
logging_checkbox.stateChanged.connect(toggle_log)

printing_checkbox = QtGui.QCheckBox('print (immediately)')
printing_checkbox.stateChanged.connect(print_to_console)
w5.addWidget(logging_checkbox, row=0, col=0)
w5.addWidget(printing_checkbox, row=0, col=1)
# w5.addWidget(lab0, row=1, col=0)
# w5.addWidget(force_select, row=1, col=1)
# w5.addWidget(lab1, row=2, col=0)
# w5.addWidget(freq_select, row=2, col=1)
# w5.addWidget(lab2, row=3, col=0)
# w5.addWidget(window_select, row=3, col=1)
w5.addWidget(zero_button, row=4, col=0)
w5.addWidget(apply_button, row=4, col=1)
d5.addWidget(w5)

def apply_settings():
    global sampling_freq, xaxis_lim, force_val, percentage
    # retrieve current settings
    sampling_freq = freq_select.value()
    xaxis_lim = window_select.value()
    force_val = int(force_select.currentText())
    # reset plot views
    # send sampling_freq & force_val to teensy
    # set buffer sizes based on xaxis_lim
    w1.reset()
    w2.reset()
    w3.reset()
    w4.reset()
    w1.recent_buffer = np.full((int(sampling_freq * xaxis_lim), 22), np.nan)
    w2.recent_buffer = np.full((int(sampling_freq * xaxis_lim), 17), np.nan)
    w3.recent_buffer = np.full((int(sampling_freq * xaxis_lim), 7), np.nan)
    w4.recent_buffer = np.full((int(sampling_freq * xaxis_lim), 17), np.nan)
    thread_manager.start(np.ceil(sampling_freq * xaxis_lim * percentage), 
                         sampling_freq * xaxis_lim,
                         w1.update, 
                         w2.update,
                         w3.update,
                         w4.update2)

apply_button.clicked.connect(apply_settings)
zero_button.clicked.connect(w1.center_trace)
zero_button.clicked.connect(w2.center_trace)

#pg.setConfigOptions(antialias=True)
win.show()
thread_manager = ThreadManager()
thread_manager.start(np.ceil(sampling_freq * xaxis_lim * percentage),
                     sampling_freq * xaxis_lim, 
                     w1.update,
                     w2.update,
                     w3.update,
                     w4.update2)

if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
