## Pre-install (general)
Install miniconda/anaconda

## Pre-install (linux)
Install libudev & libusb
Add udev.rules for teensy

## install env
conda env create -f environment.yml

## enter conda env
source activate plotosaurus

python plotosaurus.py

Notes:
In principle, everything is `pip install`able on all platforms now. I really like the isolation conda provides, though.

Why Python 3.5? At time of writing (6/28/2017), there were no wheels for hidapi on Windows/Python 3.6, and I didn't want to deal with the compiler issues.

Also in principle, Python 2.7.x should also work (in fact, I started development on 2.7.12), so if you already have an older version of Python on your system (Linux certainly), then just grab the requirements in the `environment.yml`. I think `PyQt4` is the only sticky requirement (not available on PyPi?)

Current division of labor:
 - plotosaurus.py handles GUI things and getting the thread rolling. 
 - threadmanager.py handles the QThread. The QThread does communication with HAND, optionally displays/logs the raw data, performs the necessary transformations on the raw data, and sends the raw/evaluated data for plotting. I also define a `ThreadManager`, which helps with persistent state across new threads, and maintains the connection with the HID across thread starts.
 - plotwindows.py receives data from the QThread, and handles the subplots.

 If you want a quicker way to just verify the device is plugged in and saying *something*, you can run `python test_hid.py`. This will print the data to the console, and provides a stripped-down example of basic communication (just reading, no settings yet).

 test_libusb.py, in principle, should do the same thing (as I understand it, HID uses an interrupt transfer over USB). However, given the availability of HID libraries on each platform, I suggest trying that first. No idea about better/worse performance in each case, but I suspect the HID interface will be simpler.