import hid
from struct import unpack
h = hid.device()

h.open(0x16c0, 0x486)
h.set_nonblocking(1)

#h.write([0]*64)

while True:
    d = bytearray(h.read(46))
    if d:
        print(unpack('>LhHHHHHHHHHHHHHHHHHHHH', d))

h.close()
