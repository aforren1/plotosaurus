import numpy as np
import pyqtgraph as pg

## Normalized voltage plot
class PlotWindow(pg.GraphicsLayoutWidget):
    def __init__(self, sampling_freq, xaxis_lim, type):
        super(PlotWindow, self).__init__()
        self.num_lines_per_plot = 4
        self.buffer_width = 22
        self.yaxis_range = [0,1]
        if type == 'force':
            self.num_lines_per_plot = 3
            self.buffer_width = 17
            self.yaxis_range = [-1,3]
        elif type == 'error':
            self.num_lines_per_plot = 1
            self.buffer_width = 7
            self.yaxis_range = [0, 0.1]

        self.zero_vals = np.full((self.buffer_width - 2,), 0)
        self.ever_zeroed = False
        self.dummy = np.full((40, self.buffer_width), 1)
        self.recent_buffer = np.full((sampling_freq * xaxis_lim, self.buffer_width), np.nan)

        k = 2 # skip the first two columns (timestamp and diff)
        self.curves = list()
        self.lines = [[0 for x in range(5)] for y in range(self.num_lines_per_plot)]
        for i in range(5):
            self.curves.append(self.addPlot(title='Finger ' + str(i + 1)))
            self.curves[i].showGrid(x=True, y=True, alpha=0.5)
            self.curves[i].setClipToView(True)
            self.curves[i].setRange(yRange=self.yaxis_range)
            self.curves[i].setLimits(yMin=self.yaxis_range[0], yMax=self.yaxis_range[1])
            for j in range(self.num_lines_per_plot):
                self.lines[j][i] = self.curves[i].plot(self.dummy[:,0], 
                                                       self.dummy[:,k], 
                                                       pen=pg.mkPen(
                                                           color=pg.intColor(j, hues=5, alpha=255),
                                                           width=3)
                                                      )
                k += 1
    
    def update(self, big_buffer):
        # update & plot big_buffer
        k = 2
        offset = 0.5 if self.ever_zeroed else 0
        self.recent_buffer[:] = big_buffer
        xmin = np.nanmin(self.recent_buffer[:,0])
        xmax = np.nanmax(self.recent_buffer[:,0])
        for i in range(5):
            self.curves[i].setLimits(xMin=xmin, xMax=xmax)
            for j in range(self.num_lines_per_plot):
                self.lines[j][i].setData(self.recent_buffer[::10,0],
                                         self.recent_buffer[::10,k] + offset -
                                         self.zero_vals[k-2])
                k += 1
    
    def reset(self):
        k = 2
        for i in range(5):
            self.curves[i].setLimits(xMin=0, xMax=1)
            for j in range(self.num_lines_per_plot):
                self.lines[j][i].setData(self.dummy[:,0],
                                         self.dummy[:,k])
                k += 1
    
    def center_trace(self):
        self.ever_zeroed = True
        self.zero_vals = np.nanmedian(self.recent_buffer[:,2:], axis=0)

