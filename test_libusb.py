from struct import unpack
import array
import usb.core
import usb.util

# for dev in usb.core.find(find_all=True):
#     print "  idVendor: %d (%s)" % (dev.idVendor, hex(dev.idVendor))
#     print "  idProduct: %d (%s)" % (dev.idProduct, hex(dev.idProduct))

dev = usb.core.find(idVendor=0x16c0, idProduct=0x486)
if dev is None:
    sys.exit("Could not find device.")

if dev.is_kernel_driver_active(0):
    try:
        dev.detach_kernel_driver(0)
        print "kernel driver detached."
    except usb.core.USBError as e:
        sys.exit("Could not detach kernel driver: ")
else:
    print "no kernel driver attached."

dev.reset()

# endpoint = dev[0][(1,0)][1] # bulk in (use Serial.write() commands on Teensy instead)
# endpoint_out = dev[0][(1,0)][0] # bulk out
endpoint = dev[0][(0,0)][0] # interrupt in
endpoint_out = dev[0][(0,0)][1] # interrupt out

while True:
    try:
        # interrupt data transfer
        data = endpoint.read(endpoint.wMaxPacketSize)[0:46]
        if data:
            print unpack('>LhHHHHHHHHHHHHHHHHHHHH', data)
    except usb.core.USBError:
            break

# commands can be single strings/bytes
dev.write(endpoint_out.bEndpointAddress, 'a')
usb.util.dispose_resources(dev)