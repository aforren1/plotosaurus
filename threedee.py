import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.opengl as gl

class ThreeDeePlot(gl.GLViewWidget):
    def __init__(self, sampling_freq, xaxis_lim):
        super(ThreeDeePlot, self).__init__()
        self.show()
        self.setCameraPosition(distance=4, azimuth=-90)
        self.g = gl.GLGridItem()
        self.g.scale(0.1, 0.1, 0.1)
        self.addItem(self.g)
        self.spacing = np.linspace(-1, 1, 5)

        self.first_measurement = True
        self.buffer_width = 17
        self.recent_buffer = np.full((sampling_freq * xaxis_lim, self.buffer_width), np.nan)
        self.buffer = np.full((15,), 0)
        self.prev_buffer = np.full((15,), 0)
        self.zero_vals = np.full((15,), 0)
        self.blobs = list()
        self.meshdata = gl.MeshData.sphere(rows=10, cols=10)
        for ii in range(0, 5):
            self.blobs.append(gl.GLMeshItem(meshdata=self.meshdata, smooth=True, shader='normalColor', glOptions='opaque'))
            self.blobs[ii].translate(self.spacing[ii], 0, 0)
            self.blobs[ii].scale(0.1, 0.1, 0.1)
            self.addItem(self.blobs[ii])
    def update2(self, big_buffer):
        # name mangled b/c already taken by parent class
        # expect a 1x15 array in the end
        # receive same buffer as xyz
        self.recent_buffer[:] = big_buffer
        self.prev_buffer = self.buffer
        self.buffer = np.nanmedian(self.recent_buffer[-20:,2:], axis=0)
        if np.isnan(self.buffer).any():
            self.buffer = np.full((15,), 0)
        elif self.first_measurement:
            self.first_measurement = False
            self.prev_buffer = self.buffer # first measurement is garbage, but our circles stay in the right spot
        delta = (self.buffer - self.prev_buffer)
        counter = 0
        for ii in range(0, 5):
            self.blobs[ii].translate(-delta[counter], delta[counter+1], -delta[counter+2])
            counter += 3
    
    def reset(self):
        counter = 0
        self.first_measurement = True
        for ii in range(0, 5):
            #TODO: this doesn't work properly, want to move to origin for each blob
            self.blobs[ii].translate(-self.prev_buffer[counter], -self.prev_buffer[counter+1], self.prev_buffer[counter+2])
            counter += 3
    
    # def center_trace(self):
    #     self.zero_vals = np.nanmedian(self.recent_buffer[:,2:], axis=0)

